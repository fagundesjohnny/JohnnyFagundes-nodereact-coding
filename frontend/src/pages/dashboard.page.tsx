import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import InfiniteScroll from 'react-infinite-scroll-component'

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1)
  const [param, setParam] = useState("")

  const fetchData = async () => {
    try {
      setLoading(true)
      const result = await backendClient.getAllUsers(page);
      setUsers(users.concat(result.data));
      setPage(page + 1)
    } catch (e) {}
    finally {
      setLoading(false)
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const filter = async () => {
    const result = await backendClient.getByParam(param);
    setUsers(result.data)
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <div>
        <input type="text" onChange={(event) => setParam(event.target.value)} />
        <button onClick={filter} >Search</button>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <InfiniteScroll
          dataLength={users.length} //This is important field to render the next data
          next={fetchData}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          endMessage={
            <p style={{ textAlign: 'center' }}>
              <b>Yay! You have seen it all</b>
            </p>
          }
          >
          <div>
            {users.length
              ? users.map((user) => {
                return <UserCard key={user.id} {...user} />;
              })
              : null}
          </div>
        </InfiniteScroll>
        {/*{loading ? (*/}
        {/*  <div*/}
        {/*    style={{*/}
        {/*      display: "flex",*/}
        {/*      justifyContent: "center",*/}
        {/*      alignItems: "center",*/}
        {/*      height: "100vh",*/}
        {/*    }}*/}
        {/*  >*/}
        {/*    <CircularProgress size="60px" />*/}
        {/*  </div>*/}
        {/*) : (*/}
        {/*  <div>*/}
        {/*    {users.length*/}
        {/*      ? users.map((user) => {*/}
        {/*          return <UserCard key={user.id} {...user} />;*/}
        {/*        })*/}
        {/*      : null}*/}
        {/*  </div>*/}
        {/*)}*/}
      </div>
    </div>
  );
};
