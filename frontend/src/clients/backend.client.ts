import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getByParam(value: string): Promise<{ data: IUserProps[] }> {
    return (await axios.get(`${this.baseUrl}/people/search?search=${value}`, {})).data;
  }

  async getAllUsers(page: number = 1): Promise<{ data: IUserProps[] }> {
    return (await axios.get(`${this.baseUrl}/people/all?page=${page}`, {})).data;
  }
}
