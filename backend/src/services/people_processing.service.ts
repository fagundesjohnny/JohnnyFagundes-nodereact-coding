import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    search(search: string) {
        return people_data.filter(item => item.gender === search || item.title === search || item.company === search);
    }

    getAll(page?: number, size: number = 20) {
        if (page) {
            return people_data.slice((page - 1) * size, page * size);
        }
        return people_data;
    }
}
